from collections import Counter

from pymongo import MongoClient

from util.Utils import MONGO_HOST
from util.Utils import MONGO_PORT

""" Check mongo db for duplicate twitter actor names or twitter accounts """

# maps to count actors
real_name = Counter()
twitter_name = Counter()

client = MongoClient(MONGO_HOST, MONGO_PORT)
db = client.rmoa
coll_actors = db.actors

for actor in coll_actors.find({'twitter-account': {'$exists': True}}):
    name = actor['givenname'] + ' ' + actor['surname']
    account = actor['twitter-account']

    real_name[name] += 1
    twitter_name[account] += 1

twitter_sorted = sorted([(name, count) for name, count in twitter_name.items() if count > 1],
                        key=lambda count: count[1], reverse=True)
name_sorted = sorted([(name, count) for name, count in real_name.items() if count > 1], key=lambda count: count[1],
                     reverse=True)

print('Duplicate real names')
for name, count in name_sorted:
    print('{}\t{}'.format(name, count))

print('\nDuplicate twitter names')
for name, count in twitter_sorted:
    print('{}\t{}'.format(name, count))
