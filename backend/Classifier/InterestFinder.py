import time

from nltk import FreqDist
from pymongo import MongoClient

from util.Log import Log
from util.Utils import MONGO_HOST, MONGO_PORT, TWEET_PATTERN

__author__ = 'Benedikt'
TAG = "InterestFinder"


class InterestFinder(object):
    """
    Calculates actor interests based on their most used hashtags.
    """
    def __init__(self, tweet_cursor):
        self.tweets = tweet_cursor

        # get list with (tweet, sentiment) tuples
        tw_list = [(tweet['tweet'], tweet['sent']) for tweet in self.tweets]
        self.tweets.rewind()  # reset iterator
        # get list with (hashtags, sentiment of tweet) tuples
        ht_list = [(tweet['hashtags'], tweet['sent']) for tweet in self.tweets if tweet['hashtags']]

        Log.d(TAG, "{} tweets".format(len(tw_list)))
        Log.d(TAG, "{} tweets w hashtags".format(len(ht_list)))

        self.tw_fdist = FreqDist()
        for tweet in tw_list:
            word = (word.lower() for word in TWEET_PATTERN.split(tweet[0]) if
                    len(word) > 3 and not word.startswith('#'))
            if tweet[1] == 1:
                self.tw_fdist.update(word)
            elif tweet[1] == -1:
                self.tw_fdist.subtract(word)
            else:
                Log.e(TAG, "Invalid sentiment of tweet {}: {}".format(tweet[0]), tweet[1])

        self.tw_sorted = sorted(self.tw_fdist.items(), key=lambda hashtag: hashtag[1])

        self.ht_fdist = FreqDist()

        for hashtags in ht_list:
            ht = [hashtag.lower() for hashtag in hashtags[0]]
            if hashtags[1] == 1:
                self.ht_fdist.update(ht)
            elif hashtags[1] == -1:
                self.ht_fdist.subtract(ht)
            else:
                Log.e(TAG, "Invalid sentiment of hashtag {}: {}".format(hashtags[0]), hashtags[1])

        self.ht_sorted = sorted(self.ht_fdist.items(), key=lambda hashtag: hashtag[1])

    def get_pos_ht(self, n=10):
        return [hashtag for hashtag in reversed(self.ht_sorted[-n:]) if hashtag[1] > 0]

    def get_neg_ht(self, n=10):
        return [hashtag for hashtag in reversed(self.ht_sorted[:n]) if hashtag[1] < 0]

    def print_ht_info(self):

        Log.d(TAG, "Most positive Hashtags")

        for hashtag in self.get_pos_ht():
            Log.d(TAG, hashtag)

        Log.d(TAG, "Most negative Hashtags")

        for hashtag in self.get_neg_ht():
            Log.d(TAG, hashtag)

    def print_tw_info(self):
        Log.d(TAG, "Most positive words in tweets")

        for hashtag in reversed(self.tw_sorted[-10:]):
            Log.d(TAG, hashtag)

        Log.d(TAG, "Most negative words in tweets")

        for hashtag in self.tw_sorted[:10]:
            Log.d(TAG, hashtag)


if __name__ == '__main__':
    """Runs only one times through the database, so restart it if interests should be calculated again."""

    client = MongoClient(MONGO_HOST, MONGO_PORT)
    db = client.rmoa
    coll_actors = db.actors
    coll_tweets = db.tweets

    Log.d(TAG, "Start finding interests")

    # get only actors with twitter account
    for actor in coll_actors.find({'twitter-account': {'$exists': True}}):
        acc_name = actor['twitter-account']

        tweets = coll_tweets.find({'$and': [{'account': acc_name}, {'sent': {'$exists': True}}]})

        if tweets.count() == 0:
            Log.d(TAG, "No tweets of '{}' found.".format(acc_name))
            continue

        Log.d(TAG, "Finding interests of '{}'.".format(acc_name))
        finder = InterestFinder(tweets)

        interests = finder.get_pos_ht(3)
        try:
            Log.d(TAG, interests)
        except UnicodeEncodeError:
            Log.d(TAG, "Chinese character in interests")

        interests = [interest[0] for interest in interests]
        if interests:
            res = coll_actors.update_one({'_id': actor['_id']},
                                     {'$set': {'interests': interests, 'int_time': time.time()}})

            if res.matched_count != 1:
                Log.e(TAG, "Error updating tweet")

    Log.d(TAG, "Finish finding sentiments")
