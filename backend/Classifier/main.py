import getopt
import sys

from pymongo import MongoClient

from classifier.Classifier import *
from classifier.RandomTweeter import RandomTweeter
from util.Utils import MONGO_HOST
from util.Utils import MONGO_PORT

TWEET_FILE = "data/train.csv"
RANDOM_TWEET_FILE = "data/train_random.csv"
TEST_TWEET_FILE = "data/test.csv"

SLEEP_HOUR = 6
SLEEP_TIME = SLEEP_HOUR * 60 * 60

__author__ = 'bstricker'
TAG = "main"


def new_rand_tweets(n, override=False):
    tweeter = RandomTweeter(TWEET_FILE)
    tweeter.write_random_tweets(n, RANDOM_TWEET_FILE.format(n), override)


def create_classifier(n, filename, ngram):
    new_rand_tweets(n, True)
    classifier = TweetClassifier(RANDOM_TWEET_FILE, ngram, use_smileys=True, use_stopwords=True, use_stemming=True)
    classifier.read_data()
    classifier.train()
    TweetClassifier.save(classifier, filename)
    return classifier


def print_usage():
    Log.d(TAG, "main.py <classifier>")
    Log.d(TAG, "Optional:")
    Log.d(TAG,
          "\t-n <tweets> [1, 2]\tCreate a new classifier with n training tweets and use uni- or bigrams as features")
    Log.d(TAG, "\t-s <sleep>\tSleep s hours before fetching new tweets from server")
    Log.d(TAG, "\t-h print this help")
    Log.d(TAG, "\t-f log to file")


def read_test_tweets(path):
    tweets = []
    with open(path, 'r') as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            # save tweet text with sentiment
            tweets.append((row[0], int(row[1])))
    return tweets


def run_classifier():
    """
    Start the classifier that trains with random tweet data and calculates sentiment of actor tweets from the mongo db.
    """
    global SLEEP_HOUR
    # check for mandatory file
    if len(sys.argv) < 2:
        print_usage()
        sys.exit(1)
    file = sys.argv[1]
    n_tweets = None
    ngram = None
    try:
        opts, add_args = getopt.getopt(sys.argv[2:], "hfs:n:g:")
    except getopt.GetoptError:
        print_usage()
        sys.exit(1)
    for option, opt_arg in opts:
        if option == '-h':
            print_usage()
            sys.exit()
        elif option == "-s":
            SLEEP_HOUR = opt_arg
        elif option == "-n":
            n_tweets = int(opt_arg)
            ngram = NGram(int(add_args[0]))
        elif option == "-f":
            Log.TO_FILE = True
    Log.d(TAG, "Sleep time (hours):\t{}".format(SLEEP_HOUR))
    # check if a new classifier should be created
    if n_tweets:
        Log.d(TAG, "Create new Classifier '{}' with {} training tweets".format(file, n_tweets))
        tweet_classifier = create_classifier(n_tweets, file, ngram)
    else:
        Log.d(TAG, "Use Classifier file:\t'{}'".format(file))
        tweet_classifier = TweetClassifier.load(file)
        if not tweet_classifier:
            Log.e(TAG, "Classifier {} not found. Exit!".format(file))
            sys.exit()
    client = MongoClient(MONGO_HOST, MONGO_PORT)
    db = client.rmoa
    train_size = len(tweet_classifier.train_tweets)
    tweet_classifier.info()

    # test with tweets from test.csv
    Log.d(TAG, "Test with test set from {}".format(TEST_TWEET_FILE))
    test_tweets = read_test_tweets(TEST_TWEET_FILE)
    tweet_classifier.test(test_tweets, True)
    tw_coll = db.tweets

    while True:

        Log.d(TAG, "Start analyzing tweets")

        # all tweets which are either not analyzed yet or are analyzed with an older classifier
        # (e.g. lower training size)
        cursor = tw_coll.find(
            {'$or': [{'sent_date': {'$exists': False}}, {'sent_n': {'$lt': train_size}}]}).batch_size(60)
        for i, tweet in enumerate(cursor):
            sentiment = tweet_classifier.classify(tweet['tweet'])
            res = tw_coll.update_one({'_id': tweet['_id']},
                                     {'$set': {'sent': sentiment, 'sent_date': time.time(), 'sent_n': train_size}})

            if i % 100 == 0:  # print 'progress bar'
                Log.d(TAG, ".")
            if res.matched_count != 1:
                Log.e(TAG, "Error updating tweet")

        Log.d(TAG, "Finish analyzing tweets")

        Log.d(TAG, "Sleeping for {} hours...".format(SLEEP_HOUR))
        time.sleep(SLEEP_TIME)


def test():
    """
    Runs benchmark on different classifiers.

    Use 50k random tweets and test every combination of possible classifer:
    unigram/bigrams
    use smileys     yes/no
    use stopwords   yes/no
    use stemming    yes/no

    Create for each classifier three log files. One for the standard output,
    one with the correct classifications and one with the false ones.

    """


new_rand_tweets(50000, True)

for i in range(0, pow(2, 4)):

    ngram = 1 if i & 1 else 0
    smile = 1 if i & 2 else 0
    stop = 1 if i & 4 else 0
    stem = 1 if i & 8 else 0

    ngram_ = NGram(int(1)) if ngram == 0 else NGram(int(2))

    fname = "Uni" if ngram == 0 else "Bi"
    if smile:
        fname += "_smile"
    if stop:
        fname += "_stop"
    if stem:
        fname += "_stem"

    fname += ".txt"

    Log.TO_FILE = True
    Log.GLOBAL_OUTPUT = fname

    classifier = TweetClassifier(RANDOM_TWEET_FILE, ngram_, use_smileys=smile, use_stopwords=stop,
                                 use_stemming=stem)
    classifier.read_data()
    classifier.train()
    classifier.info()

    # test with tweets from test.csv
    Log.d(TAG, "Test with test set from {}".format(TEST_TWEET_FILE))
    test_tweets = read_test_tweets(TEST_TWEET_FILE)
    classifier.test(test_tweets, True, correct_txt=fname[0:-4] + "_correct.txt",
                    false_txt=fname[0:-4] + "_false.txt")

if __name__ == '__main__':
    run_classifier()
    # test()
