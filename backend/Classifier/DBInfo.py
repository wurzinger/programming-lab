import getopt
import sys

from pymongo import MongoClient

from util.Utils import MONGO_HOST, MONGO_PORT


__author__ = 'Benedikt'

TAG = "Info"

"""
Prints information about stored data:
# tweets
# classified tweets
# non-classified tweets
# actors
# actors with twitter account
"""


def print_usage():
    print("DBInfo.py <options>")
    print("Options:")
    print("\t-t Info about stored tweets")
    print("\t-a Info about stored actors")


if __name__ == '__main__':

    # check for mandatory option
    if len(sys.argv) < 2:
        print_usage()
        sys.exit(1)

    file = sys.argv[1]

    try:
        opts, add_args = getopt.getopt(sys.argv[1:], "ta")
    except getopt.GetoptError:
        print_usage()
        sys.exit(1)

    client = MongoClient(MONGO_HOST, MONGO_PORT)
    db = client.rmoa

    tw_coll = db.tweets
    actor_coll = db.actors

    for option, opt_arg in opts:
        if option == '-t':
            print("# tweets\t\t\t\t{}".format(tw_coll.count()))
            print("# classified tweets\t\t{}".format(tw_coll.count({'sent': {'$exists': True}})))
            print("# non classified tweets\t{}".format(tw_coll.count({'sent': {'$exists': False}})))
        elif option == '-a':
            print("# actors\t\t\t\t{}".format(actor_coll.count()))
            print("# actors w twitter\t\t{}".format(actor_coll.count({'twitter-account': {'$exists': True}})))
