from random import Random
import time
from util import Utils

from util.Log import Log


__author__ = 'bstricker'
TAG = "RandomTweeter"

class RandomTweeter(object):
    """ Use to read big tweet-file and export a smaller variable file with random tweets.
    """

    def __init__(self, file):

        self.path = file
        self.tweets = self._read_data()

    def _read_data(self):

        Log.d(TAG, "Start reading tweets from {}".format(self.path))
        t = time.time()

        with open(self.path, 'r') as tweet_file:
            tweets = tweet_file.readlines()  # read all lines at one

        Log.d(TAG, "Finish reading {} tweets after {}".format(len(tweets), Utils.to_readable_time(time.time() - t)))

        return tweets

    def write_random_tweets(self, n, path, overwrite=False):

        mode = 'w'

        # prevent overwriting exiting file accidentally
        if not overwrite:
            while True:
                Log.d(TAG, "Overwrite file not allowed. Append new tweets or cancel operation?")
                Log.d(TAG, "Type 'append' or 'cancel'")
                action = input()  # wait for user input

                if action == "cancel":  # do nothing, just return
                    Log.d(TAG, "Canceled. No changes to '{}' made.".format(path))
                    return
                if action == "append":
                    mode = 'a'  # open file in 'append' mode
                    break

        Log.d(TAG, "Creating random tweets in '{}'".format(path))

        num = len(self.tweets)
        rand = Random()
        with open(path, mode) as output:
            for x in range(n):
                i = rand.randint(0, num-1)
                output.write(self.tweets[i])  # choose a random tweet

        Log.d(TAG, "Wrote {} tweets in '{}'.".format(n, path))