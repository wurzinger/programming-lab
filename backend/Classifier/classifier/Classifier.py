import csv
import pickle
import string
import time

import nltk

from classifier.NGram import NGram
from util.Log import Log
from util import Utils

__author__ = 'bstricker'
TAG = "TweetClassifier"

POS_SMILEYS = [':)', ':-)', '(:', ';)', ';-)', ':-))', ':D', ':-D', ':]', '=)', 'xD']
NEG_SMILEYS = [':(', ':-(', ':[', '):']

# Stopwords from http://www.ranks.nl/stopwords
STOP = ['a', 'about', 'above', 'after', 'again', 'against', 'all', 'am', 'an', 'and', 'any', 'are', 'as', 'at',
        'be', 'because', 'been', 'before', 'being', 'below', 'between', 'both', 'by', 'cant', 'cannot', 'could',
        'did', 'do', 'does', 'doesnt', 'doing', 'dont', 'down', 'during', 'each', 'few', 'for',
        'from', 'further', 'had', 'has', 'have', 'having', 'he', 'hed', 'hell', 'hes',
        'her', 'here', 'heres', 'hers', 'herself', 'him', 'himself', 'his', 'how', 'hows', 'i', 'id', 'ill', 'im',
        'ive', 'if', 'in', 'into', 'is', 'it', 'its', 'its', 'itself', 'lets', 'me', 'more', 'most',
        'my', 'myself', 'of', 'off', 'on', 'once', 'only', 'or', 'other', 'ought', 'our',
        'ours 	ourselves', 'out', 'over', 'own', 'same', 'shant', 'she', 'shed', 'shell', 'shes', 'should',
        'shouldnt', 'so', 'some', 'such', 'than', 'that', 'thats', 'the', 'their', 'theirs', 'them', 'themselves',
        'then', 'there', 'theres', 'these', 'they', 'theyd', 'theyll', 'theyre', 'theyve', 'this', 'those', 'through',
        'to', 'too', 'under', 'until', 'up', 'very', 'was', 'wasnt', 'we', 'wed', 'well', 'were', 'weve', 'were',
        'werent', 'what', 'whats', 'when', 'whens', 'where', 'wheres', 'which', 'while', 'who', 'whos', 'whom', 'why',
        'whys', 'with', 'wont', 'would', 'wouldnt', 'you', 'youd', 'youll', 'youre', 'youve', 'your', 'yours',
        'yourself', 'yourselves']
N_STOP = ['no']


def get_sentiment(sentiment):
    """ Converts sentiment given by the training data into our rating system.

    Training file uses  0 = negative, 2 = neutral, 4 = positive sentiment.
    We convert it into -1 = negative, 0 = neutral and 1 = positive. Although returning 0 for neutral this tweets will
    be discarded later. (use 2-Class-Classifier)
    """
    if sentiment > 2:
        return 1
    if sentiment < 2:
        return -1
    else:
        return 0


class TweetClassifier(object):
    """ Classifier to classify tweets into tweets with 'positive'(1) or 'negative'(0) sentiment.

    Use a NaiveBayesClassifier from nltk package as classifier.

    Taken from http://www.laurentluce.com/posts/twitter-sentiment-analysis-using-python-and-nltk/

    """

    def __init__(self, train_file_path, ngram, lowercase=True, old_split=False, use_smileys=False, use_stopwords=False,
                 use_stemming=False):

        self.ngram = ngram
        self.train_path = train_file_path
        self.train_tweets = []  # stores each tweet as a tuple (tweet text, sentiment)
        self.train_word_features = None  # stores each word in an array
        self.test_word_features = None  # stores each word in an array

        self.lowercase = lowercase
        self.old_split = old_split
        self.use_smileys = use_smileys
        self.use_stop = use_stopwords
        self.stemmer = nltk.stem.SnowballStemmer("english") if use_stemming else None
        self.classifier = None

    def _filter_tweet(self, tweet):

        # remove leading/trailing whitespaces
        tweet = tweet.strip()

        if self.lowercase:
            tweet = tweet.lower()

        tweet_filtered = []

        if self.old_split:
            # split only with regex
            tweets = Utils.TWEET_PATTERN.split(tweet)
            for word in tweets:
                if len(word) < 3:
                    continue
                tweet_filtered.append(word)
        else:
            # split normal and use more checks to reduce dict size
            tweets = tweet.split()
            remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)
            for word in tweets:
                if word.startswith('@'):
                    continue
                elif word.startswith('http://') or word.startswith('https://'):
                    continue

                word = word.translate(remove_punctuation_map)

                if self.use_stop:
                    if word in STOP and word not in N_STOP:
                        continue
                    if len(word) < 3 and word not in N_STOP:
                        continue
                else:
                    if len(word) < 3:
                        continue
                if word.isnumeric():
                    continue

                if self.stemmer:
                    word = self.stemmer.stem(word)
                tweet_filtered.append(word)

        if self.ngram == NGram.bigram:
            tweet_filtered = list(nltk.bigrams(tweet_filtered))

        return tweet_filtered

    def read_data(self):

        self.train_tweets = self.parse_file(self.train_path)

        self.train_word_features = self._calc_features(self._get_words_in_tweets(self.train_tweets))
        Log.d(TAG,
              "Train set:\t{} tweets\t{} different {}s".format(len(self.train_tweets), len(self.train_word_features),
                                                               self.ngram.name))

    def parse_file(self, path):
        Log.d(TAG, "Start reading tweets from '{}'".format(path))
        t = time.time()
        tweets = []
        with open(path, 'r') as file:
            reader = csv.reader(file, delimiter=',')
            for row in reader:

                # convert 0 to 4 sentiment rating into -1 (neg) and 1 (pos) classes
                sentiment = get_sentiment(int(row[0]))

                # skip neutral tweets
                if sentiment == 0:
                    continue
                # save tweet text with sentiment
                tweets.append((self._filter_tweet(row[5]), sentiment))

        Log.d(TAG, "Finish reading {} tweets after '{}'".format(len(tweets), Utils.to_readable_time(time.time() - t)))
        return tweets

    def _extract_train_feature(self, doc):

        # print error if no word features are available
        if not self.train_word_features:
            Log.e(TAG, "Word features are empty. Maybe forgot to call '_calc_features'?")

        doc_words = set(doc)
        features = {}
        for word in self.train_word_features:
            in_words = word in doc_words
            features[word] = in_words
        return features

    def _extract_test_feature(self, doc):

        # print error if no word features are available
        if not self.test_word_features:
            Log.e(TAG, "Word features are empty. Maybe forgot to call '_calc_features'?")

        doc_words = set(doc)
        features = {}
        for word in self.test_word_features:
            in_words = word in doc_words
            features[word] = in_words
        return features

    def _calc_features(self, wordlist):
        return nltk.FreqDist(wordlist)

    def _get_words_in_tweets(self, tweets):
        all_words = []
        for (words, sentiment) in tweets:
            all_words.extend(words)
        return all_words

    def train(self):

        Log.d(TAG, "Start training...")
        t = time.time()

        training_set = nltk.classify.apply_features(self._extract_train_feature, self.train_tweets)
        self.classifier = nltk.NaiveBayesClassifier.train(training_set)
        Log.d(TAG, "Finish training after {}".format(Utils.to_readable_time(time.time() - t)))

    def test(self, gold, verbose=False, correct_txt='./correct.txt', false_txt='./false.txt'):

        results = [self.classify(tweet) for (tweet, sent_truth) in gold]
        correct = []

        # print header
        if verbose:
            header = "{}\t|\t{}\t{}".format('result', 'truth', 'tweet')
            Log.d(TAG, header, file=correct_txt)
            Log.e(TAG, header, file=false_txt)

        for ((tweet, sent_truth), sent) in zip(gold, results):
            is_correct = sent_truth == sent
            if verbose:
                msg = "{}\t|\t{}\t{}".format(sent, sent_truth, tweet)
                if is_correct:
                    Log.d(TAG, msg, file=correct_txt)
                else:
                    Log.e(TAG, msg, file=false_txt)

            correct.append(is_correct)

        if correct:
            accuracy = float(sum(correct)) / len(correct)
        else:
            accuracy = 0

        Log.d(TAG, "Accuracy:\t{:.2%}".format(accuracy))

    def info(self):
        self.classifier.show_most_informative_features()

    def classify(self, tweet):

        if self.use_smileys:
            for smile in POS_SMILEYS:
                if smile in tweet:
                    return 1

            for smile in NEG_SMILEYS:
                if smile in tweet:
                    return -1

        return self.classifier.classify(self._extract_train_feature(self._filter_tweet(tweet)))

    @staticmethod
    def save(classifier, path):
        Log.d(TAG, "Save classifier to '{}'".format(path))
        with open(path, 'wb') as outputstream:
            pickle.dump(classifier, outputstream)
            Log.d(TAG, "Saving successful")

    @staticmethod
    def load(path):
        Log.d(TAG, "Load classifier from '{}'".format(path))
        try:
            with open(path, 'rb') as inputstream:
                classifier = pickle.load(inputstream)
                Log.d(TAG, "Loading successful")
                Log.d(TAG, "Train set:\t{}\t{} different words".format(len(classifier.train_tweets),
                                                                       len(classifier.train_word_features)))
                Log.d(TAG, "Test set:\t{}\t{} different words".format(len(classifier.test_tweets),
                                                                      len(classifier.test_word_features)))
                return classifier
        except FileNotFoundError as err:
            Log.e(TAG, "ERROR:\t{}".format(err))
