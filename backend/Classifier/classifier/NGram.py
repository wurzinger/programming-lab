from enum import Enum

__author__ = 'bstricker'


class NGram(Enum):
    unigram = 1
    bigram = 2