import re

__author__ = 'bstricker'

MONGO_PORT = 27017

MONGO_HOST = '138.232.66.111'

TWEET_PATTERN = re.compile("[^a-zA-Z0-9@#]+")


def to_readable_time(seconds):
    times = [('s', 60), ('min', 60), ('h', 24), ('d', -1)]

    # special more precise case
    if seconds < 5:
        return '{:.2f} s'.format(seconds)

    time_str = ''
    time = int(seconds)

    for suffix, factor in times:

        if time > factor != -1:
            mod = divmod(time, factor)
            time_str = "{} {} {}".format(mod[1], suffix, time_str)
            time = mod[0]
        else:
            return "{} {} {}".format(time, suffix, time_str)

