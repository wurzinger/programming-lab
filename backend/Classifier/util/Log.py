import time
import sys

__author__ = 'bstricker'


class Log(object):
    """
    Log class to enable formatted logging and logging to file
    """
    GLOBAL_OUTPUT = './log.txt'
    OUTPUT = './log.txt'
    ERROR_OUTPUT = './error_log.txt'
    TIME_FORMAT = "%d.%m.%y %H:%M:%S"
    SHOW_TIMESTAMP = False
    NAME_WIDTH = 15
    TO_FILE = False

    @staticmethod
    def d(tag, msg, file=OUTPUT):
        # log to file if global variable is set or a specific file is given
        log_file = Log.TO_FILE or file != Log.OUTPUT
        if log_file:
            if Log.GLOBAL_OUTPUT != Log.OUTPUT and file == Log.OUTPUT:
                file = Log.GLOBAL_OUTPUT
            out = open(file, 'a', encoding='utf-8')
        string = "{:{width}}\t{}\t{}".format(tag[:Log.NAME_WIDTH], time.strftime(Log.TIME_FORMAT), msg,
                                             width=Log.NAME_WIDTH)
        print(string)
        if log_file:
            print(string, file=out)
            out.close()

    @staticmethod
    def e(tag, msg, file=ERROR_OUTPUT):
        # log to file if global variable is set or a specific file is given
        log_file = Log.TO_FILE or file != Log.ERROR_OUTPUT
        if log_file:
            out = open(file, 'a', encoding='utf-8')
        string = "{:{width}}\t{}\t{}".format(tag[:Log.NAME_WIDTH], time.strftime(Log.TIME_FORMAT), msg,
                                             width=Log.NAME_WIDTH)
        print(string, file=sys.stderr)
        if log_file:
            print(string, file=out)
            out.close()
