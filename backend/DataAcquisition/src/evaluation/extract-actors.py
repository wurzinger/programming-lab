__author__ = 'Stefan Wurzinger'

from pymongo import MongoClient

import logging

######################################################################
# ########################### CONSTANTS ##############################
######################################################################
CONSUMER_KEY = 'LM9tXnYIS3mvUIDOFgroSYyCx'
CONSUMER_SECRET = 'P9L5tMYyjZYHnZWSxeMK3QZOXuYExIYcB4lq6YHpOAcDwPRMqZ'
ACCESS_TOKEN = '3086237067-W0IRGir0y1jwVAB2e4okiGAhVV22H1J32l4dm6T'
ACCESS_TOKEN_SECRET = 'p12nFxhqvB9xBokoxBdgHUuVbO6x3gYEUGSHZeSuyKo9F'

DB_SERVER_ADDRESS = '138.232.66.111'
DB_SERVER_PORT = 27017
######################################################################

FORMAT = '%(asctime)s:  %(levelname)-8s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT, filename="logs/extract-actors.log")
logger = logging.getLogger('tweets')

logger.info("Twitter tweets crawling started")

client = MongoClient(DB_SERVER_ADDRESS, DB_SERVER_PORT)
db = client.rmoa


try:

    count = 0

    for actor in db.actors.find().sort('date', 1):

        count += 1
        if count >= 280:
            logging.info("Actor('%s', '%s', False, '%s')", actor.get('givenname'), actor.get('surname'), actor.get('twitter-account'))
            count = 0

    logging.info("Twitter crawling successfully finished")

except Exception as e:
    logger.error("Crawler exited with an error")
    logger.error(e)

finally:
    if client is not None:
        client.close()
