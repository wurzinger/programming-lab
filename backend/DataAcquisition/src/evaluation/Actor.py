__author__ = 'Stefan Wurzinger'


class Actor:

    givenname = ''
    surname = ''
    twitter_account = ''
    is_dead = False

    def __init__(self, givenname, surname, is_dead, twitter_account):
        self.givenname = givenname
        self.surname = surname
        self.is_dead = is_dead
        self.twitter_account = twitter_account

    def get_name(self):
        return self.givenname + ' ' + self.surname
