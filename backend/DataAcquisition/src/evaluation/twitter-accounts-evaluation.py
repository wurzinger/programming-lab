__author__ = 'Stefan Wurzinger'

from src.Settings import Settings
from src.evaluation.Account import Account
from src.evaluation.GoldStandard import GoldStandard
from src.evaluation.TestConfiguration import TestConfiguration

from TwitterAPI import TwitterAPI
from pymongo import MongoClient

import datetime
import time
import logging

FORMAT = '%(asctime)s:  %(levelname)-8s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT, filename="logs/twitter-accounts-evaluation.log")
logging.info("Twitter account crawling started")

twitter = TwitterAPI(Settings.CONSUMER_KEY, Settings.CONSUMER_SECRET, Settings.ACCESS_TOKEN, Settings.ACCESS_TOKEN_SECRET)
client = MongoClient(Settings.DB_SERVER_ADDRESS, Settings.DB_SERVER_PORT)
db = client.rmoa


def twitter_request(resource, parameters):

    while True:
        try:

            response = twitter.request(resource, parameters)
            remaining_requests = int(response.headers['x-rate-limit-remaining'])
            next_limit_reset = int(response.headers['x-rate-limit-reset'])
            if remaining_requests <= 0:
                logging.info("All Twitter requests consumed! Next requests can start at: %s", datetime.datetime.fromtimestamp(next_limit_reset))
                while time.time() < next_limit_reset:
                    time.sleep(15)
            return response

        except Exception as ex:
            logging.error(ex)
            time.sleep(15)


def split_name(activated, actor):
    if activated:
        givennames = actor.givenname.split(' ')
        return givennames[0] + ' ' + actor.surname
    return actor.get_name()


def has_min_followers(activated, account):
    if activated:
        return account.followers > 5000
    return True

def is_verified(activated, account):
    if activated:
        return account.verified
    return True

def exclude_dead_person(activated, actor):
    if activated:
        return actor.is_dead
    return False

def is_duplicate(activated, accounts, account):
    if activated:
        return account.account in accounts
    return False

def has_most_followers(activated, current_account, account):
    if activated:
        return account.followers > current_account.followers
    return True

def create_test_configurations():

    configs = []
    for i in range(0, 64):
        config = TestConfiguration()
        config.exclude_dead_person = 1 if i & 1 else 0
        config.check_verified = 1 if i & 2 else 0
        config.check_min_followers = 1 if i & 4 else 0
        config.order_by_followers = 1 if i & 8 else 0
        config.ignore_duplicates = 1 if i & 16 else 0
        config.split_names = 1 if i & 32 else 0
        configs.append(config)
        logging.info("%d%d%d%d%d%d", config.split_names, config.ignore_duplicates,
                     config.order_by_followers, config.check_min_followers, config.check_verified,
                     config.exclude_dead_person)

    return configs


try:

    gold_standard = GoldStandard()

    for config in create_test_configurations():
        results = {}
        accounts_in_use = set()
        for actor in gold_standard.values:
            name = split_name(config.split_names, actor)
            # logging.info("Search twitter account for actor: %s", name)
            account = Account()
            if not exclude_dead_person(config.exclude_dead_person, actor):
                data = twitter_request('users/search', {'q': name, 'page': 1, 'count': 10})
                for user in data:
                    tmp = Account()
                    tmp.account = user['screen_name']
                    tmp.followers = user['followers_count']
                    tmp.verified = user['verified']
                    # logging.info("Possible account for %s: %s", name, tmp)

                    if has_min_followers(config.check_min_followers, tmp) \
                            and is_verified(config.check_verified, tmp) \
                            and not is_duplicate(config.ignore_duplicates, accounts_in_use, tmp)\
                            and has_most_followers(config.order_by_followers, account, tmp):
                        account = tmp
                        # logging.info("New account for %s: %s", name, account)

                    if not config.order_by_followers:
                        break

            accounts_in_use.add(account.account)
            results[actor.get_name()] = account.account

        correct = 0
        for actor in gold_standard.values:
            if actor.twitter_account.lower() == (results[actor.get_name()]).lower():
                correct += 1

        logging.info("%d %d %d %d %d %d\t%0.2f", config.split_names, config.ignore_duplicates,
                     config.order_by_followers, config.check_min_followers, config.check_verified,
                     config.exclude_dead_person, (correct * 100) / len(gold_standard.values))

    logging.info("Twitter account crawling successfully finished")

except Exception as e:
    logging.error("Crawler exited with an error")
    logging.error(e)

finally:
    if client is not None:
        client.close()
