__author__ = 'Stefan Wurzinger'

class TestConfiguration:

    split_names = False
    check_verified = False
    order_by_followers = False
    ignore_duplicates = False
    check_min_followers = False
    exclude_dead_person = False

