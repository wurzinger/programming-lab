__author__ = 'Stefan Wurzinger'


class Account:

    account = ''
    followers = 0
    verified = False

    def __str__(self):
        return "Account: " + self.account + ", followers: " + str(self.followers) \
               + ", is verifies: " + str(self.verified)
