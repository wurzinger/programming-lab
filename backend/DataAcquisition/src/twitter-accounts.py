__author__ = 'Stefan Wurzinger'

from TwitterAPI import TwitterAPI
from pymongo import MongoClient
from src.Settings import Settings
from src.UserSearchResponse import UserSearchResponse

import datetime
import time
import logging

FORMAT = '%(asctime)s:  %(levelname)-8s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT, filename="twitter-accounts.log")
logger = logging.getLogger('twitter-accounts')

logger.info("Twitter account crawling started")

twitter = TwitterAPI(Settings.CONSUMER_KEY, Settings.CONSUMER_SECRET, Settings.ACCESS_TOKEN,
                     Settings.ACCESS_TOKEN_SECRET)
client = MongoClient(Settings.DB_SERVER_ADDRESS, Settings.DB_SERVER_PORT)
db = client.rmoa


def twitter_request(resource, parameters):

    while True:
        try:

            response = twitter.request(resource, parameters)
            remaining_requests = int(response.headers['x-rate-limit-remaining'])
            next_limit_reset = int(response.headers['x-rate-limit-reset'])
            if remaining_requests <= 0:
                logger.info("All Twitter requests consumed! Next requests can start at: %s", datetime.datetime.fromtimestamp(next_limit_reset))
                while time.time() < next_limit_reset:
                    time.sleep(15)
            return response

        except Exception as ex:
            logger.error(ex)
            time.sleep(15)


try:

    accounts = set()

    for actor in db.actors.find().sort('date', 1):

        givennames = actor.get('givenname').split(' ')
        name = givennames[0] + ' ' + actor.get('surname')

        logger.info("Search twitter account for actor <%s>", name)

        response = UserSearchResponse()
        if actor.get('death-dates') is None:
            data = twitter_request('users/search', {'q': name, 'page': 1, 'count': 1})
            for user in data:
                response.account = user['screen_name']
                response.image = user['profile_image_url']
                response.verified = user['verified']
                response.followers = user['followers_count']

        if response.followers < 5000 or (response.account in accounts):
            response = UserSearchResponse()     # reset the values

        accounts.add(response.account)

        if response.account != '':
            db.actors.update_one({"dbpedia-id": actor.get('dbpedia-id')},
                                 {'$set': {"image": response.image, "verified": response.verified, "date": time.time(),
                                  "twitter-account": response.account, "followers": response.followers}}, True)
        else:
            db.actors.update_one({"dbpedia-id": actor.get('dbpedia-id')},
                                 {
                                     '$set': {"date": time.time()},
                                     '$unset': {'image': "", "verified": "", "twitter-account": "", "followers": ""}
                                 }, True)

        logger.info("Twitter account for actor <%s> is <%s>", name, response.account)

    logging.info("Twitter account crawling successfully finished")

except Exception as e:
    logger.error("Crawler exited with an error")
    logger.error(e)

finally:
    if client is not None:
        client.close()
