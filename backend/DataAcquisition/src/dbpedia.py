__author__ = 'Stefan Wurzinger'

from SPARQLWrapper import SPARQLWrapper, JSON
from pymongo import MongoClient
from src.Settings import Settings

import time
import logging

FORMAT = '%(asctime)s:  %(levelname)-8s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT, filename="dbpedia.log")

client = MongoClient(Settings.DB_SERVER_ADDRESS, Settings.DB_SERVER_PORT)
sparql = SPARQLWrapper("http://dbpedia.org/sparql")
sparql.setReturnFormat(JSON)
sparql.setTimeout(180)
db = client.rmoa


def fetch_actors():
    logging.info("Start fetching actors")

    offset = 0
    last_offset = 0
    while True:
        sparql.setQuery("""
            PREFIX owl:     <http://dbpedia.org/ontology/>
            PREFIX foaf:    <http://xmlns.com/foaf/0.1/>
            PREFIX umbel:   <http://umbel.org/umbel/rc/>
            SELECT DISTINCT ?person ?givenname ?surname
            WHERE {
                    { ?person a owl:Actor . }
                    UNION
                    { ?person a umbel:Actor . }
                    ?person foaf:givenName ?givenname;
                            foaf:surname ?surname.
            }
            ORDER BY ?person
            OFFSET %d""" % offset)

        results = sparql.query().convert()

        for entry in results["results"]["bindings"]:
            offset += 1
            resource = entry['person']['value']
            givenname = entry['givenname']['value']
            surname = entry['surname']['value']

            db.actors.update_one({ "dbpedia-id": resource},
                            { '$set': {"dbpedia-id": resource,
                              "givenname": givenname,
                              "surname": surname,
                              "date": time.time()}}, True)

        logging.info("%d new actors fetched", (offset - last_offset))

        if offset - last_offset == 0:
            break
        last_offset = offset

    logging.info("%d actors successfully fetched", offset)


def fetch_birth_dates():
    logging.info("Start fetching birth dates of actors")

    offset = 0
    last_offset = 0
    while True:
        sparql.setQuery("""
            PREFIX owl:     <http://dbpedia.org/ontology/>
            PREFIX umbel:   <http://umbel.org/umbel/rc/>
            SELECT DISTINCT ?person (group_concat(?birthDate;separator=",") as ?birthDates)
            WHERE {
                    { ?person a owl:Actor . }
                    UNION
                    { ?person a umbel:Actor . }
                    ?person owl:birthDate ?birthDate.

            }
            GROUP BY ?person
            ORDER BY ?person
            OFFSET %d
            """ % offset)
        results = sparql.query().convert()
        for entry in results["results"]["bindings"]:
            offset += 1
            resource = entry['person']['value']
            birth_dates = set(entry['birthDates']['value'].split(','))
            db.actors.update_one({"dbpedia-id": resource},
                                {'$set': {"birth-dates": list(birth_dates)}}, True)

        logging.info("%d new birth dates for actors fetched", (offset - last_offset))

        if offset - last_offset == 0:
            break
        last_offset = offset

    logging.info("%d birth dates for actors successfully fetched", offset)


def fetch_death_dates():
    logging.info("Start fetching death dates of actors")

    offset = 0
    last_offset = 0
    while True:
        sparql.setQuery("""
            PREFIX owl:     <http://dbpedia.org/ontology/>
            PREFIX umbel:   <http://umbel.org/umbel/rc/>
            SELECT DISTINCT ?person (group_concat(?deathDate;separator=",") as ?deathDates)
            WHERE {
                    { ?person a owl:Actor . }
                    UNION
                    { ?person a umbel:Actor . }
                    ?person owl:deathDate ?deathDate.

            }
            GROUP BY ?person
            ORDER BY ?person
            OFFSET %d
            """ % offset)
        results = sparql.query().convert()
        for entry in results["results"]["bindings"]:
            offset += 1
            resource = entry['person']['value']
            death_dates = set(entry['deathDates']['value'].split(','))
            db.actors.update_one({"dbpedia-id": resource},
                                 {'$set': {"death-dates": list(death_dates)}}, True)

        logging.info("%d new death dates for actors fetched", (offset - last_offset))

        if offset - last_offset == 0:
            break
        last_offset = offset

    logging.info("%d death dates for actors successfully fetched", offset)


def fetch_birth_places():
    logging.info("Start fetching birth places of actors")

    offset = 0
    last_offset = 0
    while True:
        sparql.setQuery("""
            PREFIX dbpedia: <http://dbpedia.org/>
            PREFIX owl:     <http://dbpedia.org/ontology/>
            PREFIX umbel:   <http://umbel.org/umbel/rc/>
            PREFIX prop:    <http://dbpedia.org/property/>
            PREFIX rdfs:    <http://www.w3.org/2000/01/rdf-schema#>
            SELECT DISTINCT ?person (group_concat(?birthPlace;separator=",") as ?birthPlaces)
            WHERE {
                    { ?person a owl:Actor . }
                    UNION
                    { ?person a umbel:Actor . }
                    { ?person prop:birthPlace ?birthPlace . }
                    UNION
                    { ?person prop:birthPlace ?resource .
                      ?resource a dbpedia:Resource;
                                rdfs:label ?birthPlace. }
                    FILTER(isLiteral(?birthPlace) && langMatches(lang(?birthPlace), "EN"))
            }
            GROUP BY ?person
            ORDER BY ?person
            OFFSET %d
            """ % offset)
        results = sparql.query().convert()
        for entry in results["results"]["bindings"]:
            offset += 1
            resource = entry['person']['value']
            birth_places = set(entry['birthPlaces']['value'].split(','))
            db.actors.update_one({"dbpedia-id": resource},
                                {'$set': {"birth-places": list(birth_places)}}, True)

        logging.info("%d new birth places for actors fetched", (offset - last_offset))

        if offset - last_offset == 0:
            break
        last_offset = offset

    logging.info("%d birth places for actors successfully fetched", offset)


def fetch_movies():
    logging.info("Start fetching movies of actors")

    movies = {}
    limit = 25
    offset = 0
    last_offset = 0
    movies_count = 0
    while True:
        actors = ""
        for actor in db.actors.find().sort('dbpedia-id', 1).skip(offset).limit(limit):
            resource = actor.get('dbpedia-id')  # .replace('http://dbpedia.org/resource/', 'res:')
            actors += "(<%s>) " % resource
            offset += 1

        sparql.setQuery("""
            PREFIX owl:     <http://dbpedia.org/ontology/>
            PREFIX umbel:   <http://umbel.org/umbel/rc/>
            PREFIX prop:    <http://dbpedia.org/property/>
            PREFIX rdfs:    <http://www.w3.org/2000/01/rdf-schema#>
            SELECT DISTINCT ?person ?movie ?name
            WHERE {
                    { ?movie prop:starring ?person;
                                prop:name ?name . }
                    UNION
                    { ?movie owl:starring ?person;
                                rdfs:label ?name . }
                    VALUES (?person) { %s }
                    FILTER(isLiteral(?name) && langMatches(lang(?name), "EN"))
            }
            ORDER BY ?person ?name
            """ % actors)
        results = sparql.query().convert()
        for entry in results["results"]["bindings"]:
            resource = entry['person']['value']
            movie = entry['movie']['value']
            name = entry['name']['value']
            if resource not in movies:
                movies[resource] = []
            movies[resource].append(movie)

            db.movies.update_one({"dbpedia-id": movie},
                                 {'$set': {"name": name}}, True)

            movies_count += 1

        for k, v in movies.items():
            db.actors.update_one({"dbpedia-id": k},
                                 {'$set': {"movies": v}}, True)
        movies.clear()

        if offset - last_offset == 0:
            break
        last_offset = offset

    logging.info("%d movies for actors successfully fetched", movies_count)


def fetch_spouses():
    logging.info("Start fetching spouses of actors")

    offset = 0
    last_offset = 0
    spouses = {}
    while True:
        sparql.setQuery("""
            PREFIX owl:     <http://dbpedia.org/ontology/>
            PREFIX umbel:   <http://umbel.org/umbel/rc/>
            PREFIX foaf:    <http://xmlns.com/foaf/0.1/>
            SELECT DISTINCT ?person ?spouse ?givenname ?surname (group_concat(?birthDate;separator=",") as ?birthDates) (group_concat(?deathDate;separator=",") as ?deathDates) (group_concat(?image;separator=",") as ?images)
            WHERE {
                    { ?person a owl:Actor . }
                    UNION
                    { ?person a umbel:Actor . }
                    { ?person owl:spouse ?spouse }
                    UNION
                    { ?spouse owl:spouse ?person }
                    ?spouse foaf:givenName ?givenname;
                            foaf:surname ?surname.
                    OPTIONAL { ?spouse owl:birthDate ?birthDate. }
                    OPTIONAL { ?spouse owl:deathDate ?deathDate. }
                    OPTIONAL { ?spouse owl:thumbnail ?image. }
            }
            GROUP BY ?person ?spouse ?givenname ?surname
            ORDER BY ?person ?spouse ?givenname ?surname
            OFFSET %d
            """ % offset)
        results = sparql.query().convert()

        for entry in results["results"]["bindings"]:
            offset += 1
            actor = entry['person']['value']
            spouse_id = entry['spouse']['value']
            spouse_givenname = entry['givenname']['value']
            spouse_surname = entry['surname']['value']
            spouse_birth_dates = set(entry['birthDates']['value'].split(','))
            spouse_birth_dates.discard('')
            spouse_death_dates = set(entry['deathDates']['value'].split(','))
            spouse_death_dates.discard('')
            spouse_images = set(entry['images']['value'].split(','))
            spouse_image = spouse_images.pop()

            spoused_actor = db.actors.find_one({'dbpedia-id': spouse_id})
            if spoused_actor is not None:
                spoused_actor.pop('date', None)
                spoused_actor.pop('movies', None)
                spoused_actor.pop('spouses', None)
                spoused_actor.pop('_id', None)
                db.others.update_one({"dbpedia-id": spouse_id},
                                     {'$set': spoused_actor}, True)
            else:
                values = {}
                values["dbpedia-id"] = spouse_id
                values["dbpedia-image"] = spouse_image
                values["givenname"] = spouse_givenname,
                values["surname"] = spouse_surname
                if len(spouse_birth_dates) > 0:
                    values["birth-dates"] = list(spouse_birth_dates)
                if len(spouse_death_dates) > 0:
                    values["death-dates"] = list(spouse_death_dates)
                db.others.update_one({"dbpedia-id": spouse_id},
                                     {'$set': values}, True)

            if actor not in spouses:
                spouses[actor] = set()
            spouses[actor].add(spouse_id)

        if offset - last_offset == 0:
            break
        last_offset = offset

    for k, v in spouses.items():
        db.actors.update_one({"dbpedia-id": k},
                             {'$set': {"spouses": list(v)}}, True)

    logging.info("%d spouses for actors successfully fetched", offset)


def create_relationships():
    movies = {}
    for actor in db.actors.find({"movies": {"$exists": True}}):
        for movie in actor.get("movies"):
            if movie not in movies:
                movies[movie] = []
            movies[movie].append(actor.get('dbpedia-id'))

    actors = {}
    for movie in movies:
        for actor in movies[movie]:
            if actor not in actors:
                actors[actor] = set()
            relations = set(movies[movie])
            relations.discard(actor)
            actors[actor] = actors[actor].union(relations)

    for actor in actors:
        db.relationships.update_one({"dbpedia-id": actor},
                                    {'$set': {"knows": list(actors[actor])}}, True)

    movies.clear()
    actors.clear()


def fetch_images():
    logging.info("Start fetching images of actors")

    offset = 0
    last_offset = 0
    while True:
        sparql.setQuery("""
            PREFIX owl:     <http://dbpedia.org/ontology/>
            PREFIX foaf:    <http://xmlns.com/foaf/0.1/>
            SELECT DISTINCT ?person ?image
            WHERE {
                    { ?person a owl:Actor . }
                    UNION
                    { ?person a umbel:Actor . }
                    ?person owl:thumbnail ?image.
            }
            GROUP BY ?person
            ORDER BY ?person
            OFFSET %d
            """ % offset)
        results = sparql.query().convert()
        for entry in results["results"]["bindings"]:
            offset += 1
            resource = entry['person']['value']
            image = entry['image']['value']
            db.actors.update_one({"dbpedia-id": resource},
                                 {'$set': {"dbpedia-image": image}}, True)

        logging.info("%d new images for actors fetched", (offset - last_offset))

        if offset - last_offset == 0:
            break
        last_offset = offset

    logging.info("%d images for actors successfully fetched", offset)


def delete_tuples():
    logging.info("DBpedia delete actors with no givenname or surname")

    db.actors.remove({'givenname': {'$exists': False}})
    db.actors.remove({'surname': {'$exists': False}})
    db.actors.remove({'givenname': ""})
    db.actors.remove({'surname': ""})


try:
    logging.info("DBpedia crawling started")

    fetch_actors()
    fetch_birth_dates()
    fetch_death_dates()
    fetch_birth_places()
    fetch_movies()
    fetch_spouses()
    fetch_images()
    create_relationships()

    delete_tuples()

    logging.info("DBpedia crawling successfully finished")

except Exception as e:
    logging.error("Crawler exited with an error")
    logging.error(e)

finally:
    if client is not None:
        client.close()
