__author__ = 'Stefan Wurzinger'

from TwitterAPI import TwitterAPI
from pymongo import MongoClient
from src.Settings import Settings

import datetime
import time
import logging

FORMAT = '%(asctime)s:  %(levelname)-8s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT, filename="tweets.log")
logger = logging.getLogger('tweets')

logger.info("Twitter tweets crawling started")

twitter = TwitterAPI(Settings.CONSUMER_KEY, Settings.CONSUMER_SECRET, Settings.ACCESS_TOKEN,
                     Settings.ACCESS_TOKEN_SECRET)
client = MongoClient(Settings.DB_SERVER_ADDRESS, Settings.DB_SERVER_PORT)
db = client.rmoa


def twitter_request(resource, parameters):

    while True:
        try:

            response = twitter.request(resource, parameters)
            remaining_requests = int(response.headers['x-rate-limit-remaining'])
            next_limit_reset = int(response.headers['x-rate-limit-reset'])
            if remaining_requests <= 0:
                logger.info("All Twitter requests consumed! Next requests can start at: %s", datetime.datetime.fromtimestamp(next_limit_reset))
                while time.time() < next_limit_reset:
                    time.sleep(15)
            return response

        except Exception as ex:
            logger.error(ex)
            time.sleep(15)


try:

    for actor in db.actors.find({'twitter-account': {'$exists': True}}).sort('date', 1):

        account = actor.get('twitter-account')
        name = actor.get('givenname') + ' ' + actor.get('surname')
        last_tweet = 1 if actor.get('last-tweet') is None else actor.get('last-tweet')
        tweets = twitter_request("statuses/user_timeline", {'screen_name': account, 'count': 200, 'since_id': last_tweet})
        tweet_counter = 0
        if tweets.status_code == 200:
            for tweet in tweets:

                if str(tweet['lang']).lower() != 'en':
                    logger.info("%s tweeted in language %s! So it will be skipped", name, tweet['lang'])
                    logger.info("%s", tweet['text'])
                    continue

                tweet_counter += 1
                hashtags = []
                for hashtag in tweet['entities']['hashtags']:
                    hashtags.append(hashtag['text'])

                if last_tweet < tweet["id"]:
                    last_tweet = tweet["id"]

                post = {"name": name,
                        "account": account,
                        "tweet-id": last_tweet,
                        "tweet": tweet['text'],
                        "date": time.time(),
                        "hashtags": hashtags,
                        }

                db.tweets.insert_one(post)
            logger.info("%d tweets found for %s", tweet_counter, name)
        else:
            logger.warn("Could not fetch tweets for %s", name)

        db.actors.update_one({"dbpedia-id": actor.get('dbpedia-id')},
                             {'$set': {"date": time.time(), "last-tweet": last_tweet}}, True)

    logging.info("Twitter crawling successfully finished")

except Exception as e:
    logger.error("Crawler exited with an error")
    logger.error(e)

finally:
    if client is not None:
        client.close()
