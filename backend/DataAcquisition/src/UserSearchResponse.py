__author__ = 'Stefan Wurzinger'

class UserSearchResponse:

    account = ''
    image = ''
    followers = 0
    verified = False

    def __str__(self):
        return "Account: " + self.account + ", followers: " + str(self.followers) + ", image: " + str(self.image)
