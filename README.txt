Relationship and Interest Mining of Actors
Programming Lab SS2015
23.06.2015

AUTHORS
Thomas Schmid
Katja Sonderegger
Benedikt Stricker
Stefan Wurzinger

STRUCTURE
- backend		python code to calculate sentiment, interests and crawling twitter and dbpedia data
-- Classifier	Naive-Bayes Classifier to classify tweets and calculating interests based on used hashtags
	Contains the two main scripts 'main.py' and 'InterestFinder.py' and two auxiliary scripts 'ActorCheck.py' and 'DBInfo.py'
	 - main.py				reads random tweet, trains classifier, fetches actor tweets from the mongo db and saves continuously classified tweets back to the db, contains also the benchmark procedure
	 - InterestFinder.py	retrieves classified tweets from each actor from the db and calculates based on the hashtags their interests and store them into the db
	 - ActorCheck.py		check db for duplicate twitter accounts and twitter actor names
	 - DBInfo.py			prints information about stored data: number of tweets, classified and non-classified tweets, as well as number of actors and actors with twitter account
-- DataAcquisition	fetch actor information from dbpedia, gather user accounts from Twitter and download tweets 
--- src
	 - dbpedia.py			fetch data about actors (name, birth date, death date, relationships, movies, ...) from dbpedia
	 - twitter-accounts.py	fetch twitter accounts by the actors names (uses the Twitter Search API)
	 - tweets.py			fetch tweets by a Twitter account (uses the Twitter Search API)
---- evaluation	
	  - twitter-accounts-evaluation 	start the actor to twitter account mapping algorithm evaluation 

- documentation
	-- Images	images used in proposal
	-- Proposal	latex code of the proposal
	-- Literature	read literature
	-- Templates	image templates for schedule and prototype

- frontend		contains project-homepage and graph-visualization
	 - contact.html		contact page of project-homepage 
	 - index.html		start page of project-homepage
	 - project.html		project page of project-homepage
-- pl		contains styles and frameworks for project-homepage
--- css		contains styles for project-homepage
	 - bootstrap-theme.min.css
	 - bootstrap.min.css
	 - cover.css
--- docs	contains documentation about project
	 - proposal.pdf
--- fonts	contains icon-sets
	 - glyphicons-halflings-regular
--- js		contains scripts for project-homepage
	 - bootstrap.min.js
	 - d3.min.js
	 - ie10-viewport-bug-workaround.js
-- interactive_network_demo		contains the graph-visualization
	 - index.html
--- coffee
	 - vis.coffee		the d3.js script for creation of graphs
--- css		contains styles for the visualization
	 - bootstrap.min.css
	 - reset.css
	 - style.css
--- data	contains scripts for fetching actor data from the database
	 - create_interest_data.php			precalculates and caches interest-graphs for most well-known actors
	 - create_movie_data.php			precalculates and caches movie-graphs for most well-known actors
	 - create_relationship_data.php		precalculates and caches relationship-graphs for most well-known actors
	 - actor_interests.php				fetches information about given actor from the database and creates interest-graph
	 - actor_movies.php					fetches information about given actor from the database and creates movie-graph
	 - actor_relationships.php			fetches information about given actor from the database and creates relationship-graph
---- interests			contains cached interest-graphs
---- movies				contains cached movie-graphs
---- relationships		contains cached relationship-graphs
--- js		contains scripts and libraries for graph-visualization
	 - Tooltip.js
---- libs
	 - bootstrap.min.js
	 - coffee-script.js
	 - d3.v2.min.js
	 - jquery-1.7.2.min.js
	 - modernizr-2.0.6.min.js
	

INSTALL
- General
	Install the NoSQL database MongoDB (https://www.mongodb.org/downloads)
	Register a Twitter application to get access to the Twitter system (https://apps.twitter.com/)
	Install Webserver Apache
	Install PHP (Version 5+)
- Classifier
	install python module dependencies: (can be installed through pip)
		- pymongo
		- nltk
	set the database server address and port in the \util\Utils.py file
- DataAcquisition	
	install python module dependencies: (can be installed through pip)
		- TwitterAPI
		- pymongo
		- SPARQLWrapper 
	set the database server address and port in the \src\Settings.py file
	set the Twitter application settings (consumer key, consumer secret, access token and access token secret) in the  \src\Settings.py file
- Frontend
	Put contents of the frontend directory on the Webserver
	(Optional) Run create_interest_data.php to precalculate Interest-Graphs
	(Optional) Run create_movie_data.php to precalculate Movie-Graphs
	(Optional) Run create_relationship_data.php to precalculate Relationship-Graphs
	
	
USAGE
- Classifier
	 - main.py				main.py <classifier>
							Optional:
								-n <tweets> [1, 2]	Create a new classifier with n training tweets and use uni- or bigrams as features
								-s <sleep>	Sleep s hours before fetching new tweets from server
								-h print help
								-f log to file
							Example:
								main.py 25k.classifier -f -s 12 -n 25000 1
							Note: contains also a function 'test' to run all the benchmarks shown in the evaluation

	 - InterestFinder.py	just run InterestFinder.py
							Note: finishs after one cycle, restart periodically to keep interets up-to-date
	 - ActorCheck.py		just run ActorCheck.py
	 - DBInfo.py			DBInfo.py <options>
							Options:
								-t Info about stored tweets
								-a Info about stored actors
- DataAcquisition
	 - Gather data from dbpedia and Twitter
	   Run the scripts in following order:
		1. dbpedia.py					just run dbpedia.py				 
		2. twitter-accounts.py			just run twitter-accounts.py	
		3. tweets.py					just run tweets.py				
	 - Twitter account fetching evaluation
		twitter-accounts-evaluation.py	just run twitter-accounts-evaluation.py
- Frontend
	 - Search for actors with search-fields
	 - Change graph for current actor with radio-buttons
	 - Hover on node to see information about actors