\section{Approaches}
The core points the approach of this project exhibits are the following: 
\begin{itemize}
\item \textbf{Collecting Data} Data about actors is taken from DBpedia and Twitter to get base data for sentiment analysis. Additionally relationships are extracted from the fetched information to build a network graph of actors. 
\item \textbf{Sentiment Analysis} The data (tweets) gathered from Twitter is analyzed and the most often positively used interests are taken into account to get further information on the actor. 
\item \textbf{Visualization} The visualization of the network of actors and their relationships and interests is done with HTML, CoffeeScript, JavaScript and D3. 
\end{itemize}
Further information on each of the given points is given in the following subsections.

\subsection{Collecting data}
Data from the DBpedia and Twitter platform are taken into account to determine the relationships and interests of actors. Therefore DBpedia is used to obtain basic information about actors like their names, birth dates, birth places, death dates, images, spouses and movies they starred in. All these informations can be gathered through SPARQL queries which are processed by the offered DBpedia service. With these data the Twitter platform can be queried through the Twitter Search API to identify the Twitter accounts of the actors. Further if an account exists, tweets are fetched and based on the used hashtags, the actor's interests are computed. All the data is stored in a NoSQL database and hence permanently stored. The database consists of 75~000 records which are fetched from DBpedia and more than 500~000 records obtained from Twitter. The database has a size of about 2GB.

The relationship graph of actors is obtained through exploiting the contributors of movies the actors starred in. For example Robert Downey Jr. starred in the movie Gothika as well as Halle Berry and Penélope Cruz. Due to that fact Robert Downey Jr. is related to his both contributors. 

\subsection{Sentiment analysis}
A \emph{Naive Bayes} classifier is used to determine the sentiment of each tweet. Because the backend is written in Python, the Python library \emph{Natural Language Toolkit} (NLTK) was chosen for the implementation of the sentiment analysis. Additionally, the NLTK contains already an efficient Naive Bayes classifier implementation which is trained with the Twitter corpus mentioned in \ref{subsec:sent_data}. Our classifier is trained with 100~000 randomly chosen tweets from the annotated tweet corpus. But prior to the training, the tweets have to be preprocessed to fit the classifier's input model. Each entry in the training set for the Naive Bayes classifier consists of a list of so called \emph{features} (unigrams, bigrams, trigrams, ...) and a corresponding \emph{value}. In our case each entry represents an annotated tweet and the value expresses the sentiment. Our first approach uses unigrams (one-word tokens) as the feature type. This classifier was later extended to handle bigrams (two-word tokens) as well. Both types have different adjustments to improve their performance. For example, remove stopwords, consider smileys as intensifiers or use a language stemmer. The different approaches and improvements are discussed in detail in \ref{subsubsec_sent}.

\subparagraph{Preprocessing}
\label{subpar:preprocessing}
First, every tweet is converted to lowercase to reduce the amount of different words (e.g. \emph{Interview} and \emph{interview} represent the same token). Then each tweet is splitted with a regular expression into single words. Finally every word with less than three letters is dropped to reduce the size of the dictionary further. To refer to the example tweet from figure \ref{fig:annot_tweet}, \emph{This is not a bad idea} gets transformed into the unigram feature list \emph{[this, not, bad, idea]}. The words \emph{is} and \emph{a} are dropped because both are shorter than three characters.\\

After the training phase the classifier can be used to determine the sentiment of each crawled actor tweet and returns for each input tweet either a positive or a negative sentiment. The actor tweet has to be preprocessed with the same methods as the training tweets to guarantee the correctness of the classifier.

The drawback of the unigram classifier is the fact that it cannot handle \emph{negations} (not) and \emph{intensifiers} (very, much, ...) because each word is considered individually. Which means, our unigram classifier classifies the example tweet wrongly as negative because it has been trained that \emph{bad} is used the most time in negative tweets. Therefore, a classifier was implemented that uses bigrams instead of unigrams as features. So after preprocessing the tweet, the bigram feature list is generated from the unigram list \emph{[this, not, bad, idea]} by chaining words, resulting in \emph{[(this, not), (not, bad), (bad, idea)]}. Now \emph{not} and \emph{bad} are stored as one feature and therefore the classifier identify the substring \emph{not bad} as a positive phrase.

\subparagraph{Finding interests}
To calculate the interests of an actor, it is assumed that a hashtag represents interest into something. E.g. if an actor uses the hashtag \emph{\#TheJudge} in a positive context frequently, he is probably interested in this movie. Therefore, a frequency distribution over the hashtags in all analyzed tweets of this particular actor is built. The frequency table stores for each hashtag a counter. If a hashtag occurs in a positive analyzed tweet, the counter is incremented and decremented if it is used in a negative tweet. The hashtags with the highest counter are then probably things the actor is interested in.

\subsection{Why better than state of the art?}
The data fetching algorithm doesn't have some new innovative features because it uses the Twitter and DBpedia APIs which are state of the art. The sentiment analysis algorithm is based on already established machine learning approaches (Naive Bayes) and thus is already state of the art too.


\subsection{Visualization}
For the user interface HTML, CSS and JavaScript are used, as well as Coffeescript and D3. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=450pt]{menu.png}
    \caption{Different options and search fields.}
    \label{menu}
\end{figure}
\newpage
\noindent The user interface consists of a search field to search for actors in the current graph, a field to get the graph of another actor and the network visualization of the data. The different search fields and options can be seen in figure \ref{menu}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{example_after_search.png}
    \caption{Highlighted nodes after search.}
    \label{project-search}
\end{figure}


\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{example_after_hovering.png}
    \caption{Project UI.}
    \label{project-ui}
\end{figure}
\noindent The search field for plain text search highlights nodes of the graph, which is shown in figure \ref{project-search}. If a node is selected, more information about the actor is shown as one can see in figure \ref{project-ui}. Additionally a graph for another actor can be shown by searching for the actor by his name as shown in figure \ref{graph-actor}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{search_recommendation.png}
    \caption{Get graph for another actor.}
    \label{graph-actor}
\end{figure}
\noindent The user can choose between three different graph representations - the network of an actor according to his movies, relationships or twitter interests. This can be chosen by clicking the wanted radio-button.
\noindent In the visualization Bootstrap \footnote{http://getbootstrap.com/ - 06/14/15} is used, as well as the JavaScript Tooltip.js and some network visualization parts are taken from Vallandingham \footnote{http://flowingdata.com/2012/08/02/how-to-make-an-interactive-network-visualization/ - 06/14/15}. \\

\noindent The creation of the different graphs is done on the server via PHP scripts that can be called with the name of an actor as parameter. The scripts gather the needed information about the specified actor and then continue to search all persons related to him based on the desired connection type (movies, relationships or interests) and create the list of connections for the graph. This step is repeated for all actors that where found until the graph reaches a specified maximum depth (default value: 2). 

\noindent As there are about 15.000 actors in the database, the loading of a new graph for a certain actor would take some time if he is very well known (about 10 to 30 seconds). To overcome that obstacle the graphs of the most well known 1.000 actors are already pre-calculated and stored on the server while the graphs of the less known actors can be loaded in real-time, as they don't have that many connections.

\subsection{Evaluation}
\subsubsection{Twitter account mining}
Due to the fact that DBpedia doesn't offer any information about the Twitter accounts of actors, they were gathered directly from Twitter. Twitter provides a simple relevance-based search interface to public user accounts which can be accessed through the Twitter Search API. The account of an actor can be determined by the actors name. Each user account search results in a relevance sorted list of possible matching accounts even if the actor doesn't have one. Because of this issues we need to figure out if one of the possible matching results is suitable or not. In the naive approach we just queried for each actor the Twitter system to fetch an account. From the retrieved result set we picked the first account and assigned it to the associated actor. Of course this is a really naive strategy but it serves as a good comparison base. Several techniques have been developed and evaluated to achieve a better actor to Twitter account mapping accuracy. Following techniques have been developed:

\begin{enumerate}
\item Search for an account only if the actor is alive
\item Pick the first retrieved account only if the account is verified by Twitter - if the first retrieved account is not verified, the actor has no account
\item Pick the first retrieved account only if the actor's account has at least 5000 followers on Twitter  - if the first retrieved account has less than 5000 followers, the actor has no account
\item Pick the account from the result set which has the most followers
\item Pick the first retrieved account only if it is not already used by another actor in our database - if the first retrieved account is already in use, the actor has no account
\item If an actor has multiple fornames just use the first forname and the surname for searching. E.g. instead of searching for Kiefer William Frederick Dempsey George Rufus Sutherland search only for Kiefer Sutherland 
\end{enumerate}

To evaluate our approaches we created a manually labeled gold standard, containing one hundred randomly chosen actors. Each gold standard entry consists of a first and second name of the actor, an information if the actor is still alive and the corresponding Twitter account if one exists. Because of the fact that all mentioned techniques can be combined with each other we built an evaluation program which computes all possible combinations of these techniques and evaluate each of it with the available gold standard. That's why we come up with 64 evaluation cases which are shown in table \ref{twitter-account-evaluation}. Furthermore the table shows for each test combination how many Twitter accounts have been determined correctly (in \%). Each combination value consists of six positions who indicate which techniques are activated (1) or deactivated (0).
The list numbering from above associates a technique to each position starting from the rightmost position (e.g. 000100 indicates that an account must have at least 5000 followers to be a valid account).

\begin{table}
	\centering
	\begin{tabular}{ | l | r || l | r || l | r | }
		\hline
		Combin.&Accuracy [\%]&Combin.&Accuracy [\%]&Combin.&Accuracy [\%]\\ \hline
		000000&40.00&000001&64.00&000010&57.00\\ \hline
		000011&69.00&000100&58.00&000101&74.00\\ \hline
		000110&57.00&000111&69.00&001000&38.00\\ \hline
		001001&62.00&001010&56.00&001011&69.00\\ \hline
		001100&56.00&001101&72.00&001110&55.00\\ \hline
		001111&68.00&010000&56.00&010001&70.00\\ \hline
		010010&73.00&010011&75.00&010100&74.00\\ \hline
		010101&80.00&010110&73.00&010111&75.00\\ \hline
		011000&47.00&011001&67.00&011010&72.00\\ \hline
		011011&75.00&011100&71.00&011101&78.00\\ \hline
		011110&72.00&011111&75.00&100000&42.00\\ \hline
		100001&66.00&100010&63.00&100011&72.00\\ \hline
		100100&62.00&100101&76.00&100110&63.00\\ \hline
		100111&72.00&101000&39.00&101001&63.00\\ \hline
		101010&61.00&101011&71.00&101100&59.00\\ \hline
		101101&72.00&101110&60.00&101111&70.00\\ \hline
		110000&53.00&110001&70.00&110010&73.00\\ \hline
		110011&76.00&110100&73.00&110101&80.00\\ \hline
		110110&74.00&110111&76.00&111000&42.00\\ \hline
		111001&65.00&111010&73.00&111011&76.00\\ \hline
		111100&68.00&111101&76.00&111110&77.00\\ \hline
		111111&75.00& & & & \\
		\hline
	\end{tabular}
    \caption{Evaluation of the Twitter account mining.}
    \label{twitter-account-evaluation}
\end{table}

As we can see in the naive approach we have an accuracy of 46\% which is not the worst accuracy at all. We get the worst accuracy if we always pick the account with the most followers. From this outcome we can conclude that we shouldn't change the result ordering which is defined by the Twitter system. We get an accuracy increasement of 24\% if dead people are excluded from the Twitter account search and don't get an account assigned. This is due to the fact that the bulk part of the fetched actors from DBpedia already died and never had or don't have a Twitter account anymore. The best accuracy can be obtained through the following techniques: don't search for an account if an actor is already dead, exclude accounts which have less than 5000 followers, check if the account is already in use by another actor and search by the first forename plus the surname. With these techniques we achieve an accuracy of 80\%.  

\subsubsection{Sentiment analysis}
\label{subsubsec_sent}
To determine the performance of the implemented sentiment analysis algorithm, the \emph{accuracy}\footnote{$accuracy = \frac{\#(\mbox{correct classifications})}{\#(\mbox{all classifications})}$} as well as further properties like \emph{training time} and \emph{feature size} are compared with different versions of our own classifier. We compute the accuracy of the classifier by using the test set included in the Sentiment140 test corpus. This test set contains originally 498 annotated tweets. In contrast to the training corpus, this test set contains smileys to represent real tweets. Because we build a two-class classifier (positive vs. negative), we removed all neutral test tweets and came up with 359 tweets. Then we examined the label of each tweet and changed some sentiments and removed some of the tweets which are in our opinion neutral, although they are labeled as positive/negative. In the end, we had a test set of 350 hand labeled tweets. Thus, we can use the test set as the gold standard to test if the sentiment computed with our classifier conforms with the already annotated sentiment. We trained each classifier with the same 50~000 tweets which were randomly created at the beginning.

In general, we used two types of classifier: A classifier that uses unigrams as training features and one that uses bigrams as features. Both types are using a Naive Bayes classification and the same preprocessing stated in \ref{subpar:preprocessing}. Each classifier can also have on or more of the following modifications:

\subparagraph{Stopword removal} In the training phase, the classifier skips all words with no meaning. We used the \emph{Default English stopwords list} from  \footnote{http://www.ranks.nl/stopwords - 06/14/15} and removed for us relevant words like \emph{no}, \emph{nor} or \emph{not} from the list.

\subparagraph{Emoticon} Similar to the approach used to create the Sentiment140 corpus, we assume that every tweet with a positive smiley is also a positive tweet and a tweet with a negative smiley is a negative tweet, regardless of the output of the classifier. The following two lines list the used smileys.\\

\begin{tabular}{cccccccccccc}
	Positive & :) & :-) & (: & ;) & ;-) & :-)) & :D & :-D & :] & =) & xD \\ 
	Negative & :( & :-( & ): & :[ \\ 
\label{tab:smileys}
\end{tabular} 

\subparagraph{Stemming} Finally, we use NLTK's implementation of the \emph{English Snowball Stemmer}\footnote{http://snowball.tartarus.org/ - 06/14/15}. A stemmer reduces a given word to its word stem. E.g. \emph{translating} will be reduced to \emph{translat}. This improvement decreases the feature size  and should improve the overall performance.\\

Figure \ref{fig:class_eval} depicts the comparison between the different classifiers according to their feature size and accuracy, which will be explained in the following paragraphs.

\begin{figure}[htb]
    \centering
    \includegraphics[width=\textwidth]{classifier_evaluation.jpg}
    \caption{Classifier evaluation.}
    \label{fig:class_eval}
\end{figure}


\subparagraph{Unigram classifier} Our first approach, the simple classifier using unigrams, scores an accuracy about 81 \% (i.e. only one out of five tweets is classified wrong). The training phase took for almost all improvements (except when using the stemming mode) about 40 minutes, mostly to the fact that the NLTK's Naive Bayes implementation doesn't use parallelism. The feature size stays with $\approx$ 41~000 unique features most time the same.

One improvement is the usage of the stemming feature (+2 \% accuracy). If we use stemming we can improve our accuracy, as well as reducing the feature size by approx. 7~000 unigrams because more words correspond to the same feature. Another positive side effect is the slightly reduced training time to 34 minutes because the classifier has to train less features.

The second accuracy improvement can be achieved by using emoticons (+2 \%), which is probably due the fact that most test tweets contain emoticons. It is hard to estimate how this improvement will propose in real condition. E.g. how much smileys are really used by the actors. The advantage is, that it will only improve our classifier because we can only get correct classifications when looking at smileys.

The reason for the accuracy diminishing when filtering stopwords (-2 \%), is probably due the limitation of 140 characters in every tweet. If we shorten the already short messages by removing stopwords, we obtain a too small feature list that does not represent the sentiment of the tweet correctly.

The best result scores the classifier when using smileys and stemming. It has the highest accuracy and is due the stemming one of the fastest classifiers (35 min training time).

\subparagraph{Bigram classifier}
If we use bigrams instead of unigrams, the accuracy of our classifier drops to about 70 \%. At this time, we do not know the exact reason for this decrease but we think it is because with bigrams, we got way more different features (more possible combinations with two words) and then the probability of having enough training cases for all features, decreases. E.g. if we use unigrams and want to classify a tweet containing the word \emph{bad} the classifier has learned that bad is most time used in negative sentiments. In the bigram classifier, the word bad is used in conjunction with another word (e.g. \emph{not} bad, \emph{very} bad, bad \emph{times}) and therefore a lot more features with less trained cases are possible. For this reason the feature size is more than five times higher (about 240~000 bigrams) and the time to train the classifier almost seven times higher (4 h 50 min) than the respective unigram values. Maybe increasing the training size could overcome the problem with too less trained bigrams, but then we need an unreasonable high amount of training time as the training time is quadratic to the training size.

The different improvements affect the bigram classifier mostly the same way as the unigram classifier with the difference that stemming does not improve the accuracy at all. The usage of stopwords has one major effect on the classifier: Beside of decreasing the accuracy it reduces the feature size dramatically (- 25~000 bigrams). In combination with the stemming mode, further 12~000 features can be removed. These two improvements reduce the training time to 4 hours. But this training time is still seven times higher than the fastest unigram classifier.

Because the bigram classifier has no crucial benefit in comparison with the unigram classifier, we chose the best unigram classifier which uses emoticons and stemming.