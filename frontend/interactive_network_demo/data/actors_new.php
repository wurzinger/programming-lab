<?php

//################# new ##################
	//######### MongoDB #########
		$m = new MongoClient(); // connect
		$db = $m->rmoa;
		$collection = $db->actors;
		$moviedb = $db->movies;
		$reldb = $db->relationships;
	//######### MongoDB #########

	$actors = $collection->find(array('image'=>array('$exists'=>true),'movies'=>array('$exists'=>true),'birth-dates'=>array('$exists'=>true)))->limit(30);
	$result = array();
	$nodes = array();
	$links = array();
	foreach($actors as $actor){
		if(!in_array($actor['dbpedia-id'],$nodes)){
			$nodes[] = array(name => $actor['givenname']." ".$actor['surname'], image => $actor['image'], id => $actor['dbpedia-id'], interests => $actor['interests'] ? $actor['interests'] : "", birthday => $actor['birth-dates'][0], starring => getMovies($actor['movies']), radiusSize => rand(1,100));
			foreach(getRelations($actor['dbpedia-id']) as $friend){
				if(!in_array($friend,$nodes)){
					$tmp = $collection->find(array('dbpedia-id'=>$friend,'image'=>array('$exists'=>true),'movies'=>array('$exists'=>true),'birth-dates'=>array('$exists'=>true)))->limit(1);
					if($tmp->hasNext()){
						$tmp->next();
						$test = $tmp->current();
						$nodes[] = array(name => $test['givenname']." ".$test['surname'], image => $test['image'], id => $test['dbpedia-id'], interests => $test['interests'], birthday => $test['birth-dates'][0], starring => getMovies($test['movies']), radiusSize => rand(1,100));
						$links[] = array(source => $actor['dbpedia-id'] , target => $friend);
					}
				}else{
					$links[] = array(source => $actor['dbpedia-id'] , target => $friend);
				}
			}
		}
	}
	$result['nodes'] = $nodes;
	$result['links'] = $links;
	echo json_encode($result);

	
function getMovies($movies){
	global $moviedb;
	
	$names = array();
	$count = 0;
	foreach($movies as $movie){
		$tmp = $moviedb->find(array('dbpedia-id'=>$movie))->limit(1);
		$tmp->next();
		$test = $tmp->current();
		$names[] = $test['name'];
		if(++$count == 3) break;
	}
	return $names;
}

function getRelations($source){
	global $reldb;
	$tmp = $reldb->find(array('dbpedia-id'=>$source))->limit(1);
	$tmp->next();
	$test = $tmp->current();
	return $test['knows'];
}