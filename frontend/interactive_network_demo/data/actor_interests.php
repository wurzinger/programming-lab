<?php

//################# new ##################
	//######### MongoDB #########
		$m = new MongoClient(); // connect
		$db = $m->rmoa;
		$collection = $db->actors;
		$moviedb = $db->movies;
		//$reldb = $db->relationships;

	//######### MongoDB #########
	$name =  $_GET['name'];
	$arr = split("_", $name);
	$givenname = $arr[0];
	$surname = $arr[1];
	echo $givenname." ".$surname;
	$actors = $collection->find(array('givenname'=>$givenname,'surname'=>$surname))->limit(1);
	$result = array();
	$nodes = array();
	$links = array();
	$maxSteps = 1;
	//$radius = 1;
	foreach($actors as $actor){
		if(!inNodes($actor['dbpedia-id'])){
			$nodes[] = array(name => $actor['givenname']." ".$actor['surname'], image => $actor['image'], id => $actor['dbpedia-id'], interests => $actor['interests'] ? $actor['interests'] : "", birthday => $actor['birth-dates'][0], starring => getMovies($actor['movies']), twitter => $actor['twitter-account']);
			foreach(getRelations($actor['dbpedia-id'],$actor['interests']) as $friend){
				addActor($actor['dbpedia-id'],$friend,0);
			}
		}
	}
	$tmpArray1 = array();
	foreach($nodes as $node){
		if(!in_array($node,$tmpArray1)){
			$tmpArray1[] = $node;
		}
	}
	$tmpArray2 = array();
	foreach($tmpArray1 as $node){
		$node['radiusSize'] = rand(1,100);
		$tmpArray2[] = $node;
	}
	$result['nodes'] = $tmpArray2;
	$result['links'] = $links;
	if( file_put_contents('actors/'.$givenname.'_'.$surname.'.json',json_encode($result))) echo " - success";
	else echo " - failure";
	//echo json_encode($result);

function addActor($source,$friend,$step){
	global $nodes,$links,$maxSteps,$collection;
	if(!inNodes($friend)){
		$tmp = $collection->find(array('dbpedia-id'=>$friend,'image'=>array('$exists'=>true,'$ne'=>''),'movies'=>array('$exists'=>true,'$ne'=>''),'birth-dates'=>array('$exists'=>true,'$ne'=>'')))->limit(1);
		if($tmp->hasNext()){
			$tmp->next();
			$test = $tmp->current();
			$nodes[] = array(name => $test['givenname']." ".$test['surname'], image => $test['image'], id => $test['dbpedia-id'], interests => $test['interests'], birthday => $test['birth-dates'][0], starring => getMovies($test['movies']), twitter => $test['twitter-account']);
			$links[] = array(source => $source , target => $friend);
			if($step<$maxSteps){
				$counter = 0;
				foreach(getRelations($test['dbpedia-id'],$test['interests']) as $friend1){
					addActor($test['dbpedia-id'],$friend1,$step+1);
					if(++$counter == 7) {
						$counter=0;
						break;
					}
				}
			}
		}
	}/*else{
		$links[] = array(source => $source , target => $friend);
		if($step<$maxSteps){
			$counter = 0;
			foreach(getRelations($friend) as $friend1){
				addActor($friend,$friend1,$step+1);
				if(++$counter == 7) {
					$counter=0;
					break;
				}
			}
		}
	}*/
}

function inNodes($id){
	global $nodes;
	return in_array($id,$nodes);
}

function getMovies($movies){
	global $moviedb;
	
	$names = array();
	$count = 0;
	foreach(array_unique($movies) as $movie){
		$tmp = $moviedb->find(array('dbpedia-id'=>$movie))->limit(1);
		$tmp->next();
		$test = $tmp->current();
		$names[] = $test['name'];
		if(++$count == 3) break;
	}
	return $names;
}

/*function getRelations($source){
	global $reldb;
	$tmp = $reldb->find(array('dbpedia-id'=>$source))->limit(1);
	$tmp->next();
	$test = $tmp->current();
	return $test['knows'];
}*/

function getRelations($source,$interests){
	global $collection;
	$test = array();
	foreach($interests as $interest){
		$tmp = $collection->find(array('interests'=>$interest,'dbpedia-id'=>array('$ne'=>$source),'image'=>array('$exists'=>true,'$ne'=>''),'movies'=>array('$exists'=>true,'$ne'=>''),'birth-dates'=>array('$exists'=>true,'$ne'=>'')))->limit(10);
		
		while($tmp->hasNext()){
			$tmp->next();
			
			$actor = $tmp->current();
			
			$test[] = $actor['dbpedia-id'];
			
		}
	}
	return $test;
}