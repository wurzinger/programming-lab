<?php

	//######### MongoDB #########
		$m = new MongoClient(); // connect
		$db = $m->rmoa;
		$collection = $db->tweets;
	//######### MongoDB #########
	
	$account = isset($_GET['account']) ? $_GET['account'] : '';
	$tweets = $collection->find(array('account'=>$account));

	$result = array();
	while($tweets->hasNext()){
		$tweets->next();
		$result[] = $tweets->current();
	}
	echo json_encode($result);